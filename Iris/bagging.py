import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from iris_multi_class_scratch import Classifier
from collections import Counter
from sklearn.metrics import accuracy_score
np.random.seed(42)

if __name__ == "__main__":
    iris_df = load_iris()
    # print(iris_df["target_names"])
    X = iris_df["data"]
    y = iris_df["target"]
    # print(X.mean(axis=0), X.std(axis=0))
    X = (X - X.mean(axis=0)) / X.std(axis=0)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    # Row and Column sampling with replacement
    row_len = X_train.shape[0]
    col_len = X_train.shape[0]
    # row sampling

    # specify number of weak classifer
    n_weak_classifier = 5
    weak_classifier_dict = {}
    for wc in range(n_weak_classifier):
        weak_classifier_dict[wc] = []
        idx_list = np.random.randint(
            row_len, size=np.random.randint(low=0, high=row_len))
        instances = X_train[idx_list, :]
        targets = y_train[idx_list]
        clf_1 = Classifier()
        clf_1.fit(instances, np.where(targets == 0, 1, -1))
        clf_2 = Classifier()
        clf_2.fit(instances, np.where(targets == 1, 1, -1))
        clf_3 = Classifier()
        clf_3.fit(instances, np.where(targets == 2, 1, -1))
        weak_classifier_dict[wc] = [clf_1, clf_2, clf_3]

    pred_wc = {}
    for key, clf_arr in weak_classifier_dict.items():
        pred_wc[key] = []
        temp = []
        for clf in clf_arr:
            temp.append(clf.predict(X_test))
        for pred_1, pred_2, pred_3 in (zip(temp[0], temp[1], temp[2])):
            result = f"{pred_1}{pred_2}{pred_3}"
            if result == "1-1-1":
                pred_wc[key].append(0)
            elif result == "-11-1":
                pred_wc[key].append(1)
            elif result == "-1-11":
                pred_wc[key].append(2)
            else:
                # undefined class
                pred_wc[key].append(np.random.choice([0, 1, 2]))
    final_pred = []
    for pred_1, pred_2, pred_3 in zip(pred_wc[0], pred_wc[1], pred_wc[2]):
        ans = Counter([pred_1, pred_2, pred_3]).most_common()
        final_pred.append(ans[0][0])
    print("Final Score: ", accuracy_score(y_test, final_pred) * 100)
