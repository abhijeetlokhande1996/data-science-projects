from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import numpy as np
np.random.seed(42)


class Classifier(object):
    def __init__(self):
        self.w = None
        self.b = None

    def fit(self, x, y, epochs=300, eta=0.01):
        # self.w = np.random.normal(size=(x.shape[1], 1))
        self.w = np.zeros(shape=(x.shape[1], 1))
        self.b = 0
        for _ in range(epochs):
            for idx, ip in enumerate(x):
                ip = ip.reshape(-1, 1)
                target = y[idx]
                pred = (self.w.T @ ip + self.b)
                pred = pred.reshape(-1)
                pred = pred[0]
                pred = 1 if self.sigmoid(pred) > 0.5 else -1
                gradient = pred - target
                self.w -= eta * gradient * ip
                self.b -= eta * gradient

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def predict(self, x):
        pred_list = []
        for ip in x:
            ip = ip.reshape(-1)
            pred = self.w.T @ ip + self.b
            pred = pred.reshape(-1)
            pred = pred[0]
            pred = 1 if pred > 0 else -1
            pred_list.append(pred)
        return pred_list


def main():
    iris_df = load_iris()
    # print(iris_df["target_names"])
    X = iris_df["data"]
    y = iris_df["target"]
    # print(X.mean(axis=0), X.std(axis=0))
    X = (X - X.mean(axis=0)) / X.std(axis=0)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    # class encoding
    # 0 - setosa
    # 1 - versicolor
    # 2 - virginica
    # first recongnise setosa vs non setosa
    # second recognise versicolor vs non versicolor
    # third recognise virginica vs non verginica
    # clf_1 -> recognise only setosa and non setosa
    clf_1 = Classifier()

    clf_1.fit(X_train, np.where(y_train == 0, 1, -1))
    y_pred = clf_1.predict(X_test)
    ground_truth = np.where(y_test == 0, 1, -1)
    print("Score 1: ", accuracy_score(ground_truth, y_pred))
    # clf_2 -> recognise only versicolor and non versicolor
    clf_2 = Classifier()
    clf_2.fit(X_train, np.where(y_train == 1, 1, -1), epochs=400)
    y_pred = clf_2.predict(X_test)
    ground_truth = np.where(y_test == 1, 1, -1)
    print("Score 2: ", accuracy_score(ground_truth, y_pred))

    clf_3 = Classifier()
    clf_3.fit(X_train, np.where(y_train == 2, 1, -1))
    y_pred = clf_3.predict(X_test)
    ground_truth = np.where(y_test == 2, 1, -1)
    print("Score: 3", accuracy_score(ground_truth, y_pred))
    # Code the output (Binary Encoded Approach)
    # Setosa = 1 -1 -1
    # virginica = -1 1 -1
    # versicolor = -1 -1 1
    # rest of all cases are undefined
    final_pred_list = []
    pred_1_arr = clf_1.predict(X_test)
    pred_2_arr = clf_2.predict(X_test)
    pred_3_arr = clf_3.predict(X_test)

    arr_len = len(pred_1_arr)
    for idx in range(arr_len):
        pred_1 = pred_1_arr[idx]
        pred_2 = pred_2_arr[idx]
        pred_3 = pred_3_arr[idx]
        result = f"{pred_1}{pred_2}{pred_3}"
        if result == "1-1-1":
            final_pred_list.append(0)
        elif result == "-11-1":
            final_pred_list.append(1)
        elif result == "-1-11":
            final_pred_list.append(2)
        else:
            # undefined class
            final_pred_list.append(np.random.choice([1, 2]))

    # print(final_pred_list)
    # print(y_test)
    print("Final Score: ", accuracy_score(y_test, final_pred_list))


if __name__ == "__main__":
    main()
    pass
