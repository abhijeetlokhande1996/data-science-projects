
import numpy as np

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score


def scale_data(X):
    mean = np.mean(X, axis=0)
    std = np.std(X, axis=0)
    X_scale = (X - mean) / std
    return X_scale


def fit(X, y):
    w = np.zeros((X.shape[1], 1))
    bias = 0
    eta = 0.01
    epoch = 1000
    for e in range(epoch):
        for idx, ip in enumerate(X):
            ip = ip.reshape(-1, 1)
            target = y[idx]
            pred = w.T @ ip + bias
            pred = pred.reshape(-1)
            pred = pred[0]
            gradient = pred - target
            w = w - eta * gradient * ip
            bias = bias - eta * gradient
    return w, bias


if __name__ == "__main__":
    df = pd.read_csv("./AAPL.csv")

    df["Date"] = pd.to_datetime(df["Date"])
    df = df.set_index(df["Date"])
    print(df.isna().sum())
    X = df[["Open", "High", "Low",  "Volume"]].values
    y = df["Close"].values
    X = scale_data(X)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    w, b = fit(X_train, y_train)
    y_pred = []
    for ip in X_test:
        pred = w.T @ ip + b
        pred = pred.reshape(-1)
        pred = pred[0]
        y_pred.append(pred)
    print("Score: ", r2_score(y_test, y_pred) * 100)
